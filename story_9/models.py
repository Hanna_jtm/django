from django.db import models

# Create your models here.
class data_login(models.Model):
    username = models.CharField(max_length=20, null=True)
    email = models.EmailField(max_length=50, primary_key=True, unique=True)
    password = models.CharField(max_length=12)