from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from .models import data_login
from .forms import form_login
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

# Create your views here.
response = {}
def index(request):
    status_login = 'username' in request.session
    Form = form_login()
    if status_login:
        response['message_success'] = 'Selamat datang, ' +  request.session['username']
    response['status_login'] = status_login
    response['form'] = Form
    return render(request, 'login.html', response)

def log_in(request):
    print("login")
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        validasi = data_login.objects.filter(pk=email)
        if len(validasi) != 0:
            request.session['username'] = username
            request.session['user_email'] = email
            request.session['user_password'] = password
            return redirect('/story9/')
        else :
            messages.error(request, 'Username/email/password salah')
            return redirect('/story9/')

def log_out(request):
    request.session.flush()
    return redirect('/story9/')