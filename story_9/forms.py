from django import forms

class form_login(forms.Form):
    username = forms.CharField(required=True, max_length=20, widget=forms.TextInput(attrs={'type' : 'username', 'placeholder' : 'your_name', 'label' : 'username',  'id' : 'username'}))
    email = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'type' : 'email', 'placeholder' : 'example@email.com', 'label' : 'email', 'id' : 'email'}))
    password = forms.CharField(required=True, min_length=8, max_length=12, widget=forms.TextInput(attrs={'type' : 'password', 'placeholder' : 'Password', 'label' : 'Password', 'id' : 'password'}))