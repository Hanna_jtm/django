from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Unit Tests
class UnitTest(TestCase):
    def test_url_slash_story9_slash_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_url_slash_story9_slash_log_out_slash_is_exist(self):
        response = Client().get('/story9/log_out/')
        self.assertEqual(response.status_code, 302)

    def test_form_is_exist(self):
        response = Client().get('/story9/').content.decode('utf-8')
        self.assertIn('form', response)

    def test_button_log_in_is_exist(self):
        response = Client().get('/story9/').content.decode('utf-8')
        self.assertIn('Log in', response)

    # def test_button_log_out_is_exist(self):
    #     post = Client().post('/story9/log_in/', data={'username': 'pewe', 'email': 'pewe@mail.com', 'password':'pewe1234'})
    #     response = post.content.decode('utf-8')
    #     self.assertIn('Log out', response)

# Functional Tests
class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status_is_correct(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        response = selenium.get('http://127.0.0.1:8000/story9/')

        # Mencari sebuah object di html berdasarkan id/class/name
        form_username = selenium.find_element_by_id('username')
        form_email = selenium.find_element_by_id('email')
        form_password = selenium.find_element_by_id('password')
        button_log_in = selenium.find_element_by_id('login')

        # isi form dengan data
        form_username.send_keys('pewe')
        form_email.send_keys('pewe@mail.com')
        form_password.send_keys('pewe1234')

        # post data formnya
        button_log_in.send_keys(Keys.RETURN)
        
        # Check hasil yang dikembalikan
        self.assertIn('Selamat datang, pewe', selenium.page_source)

        # Check button log out
        button_log_out = selenium.find_element_by_id('logout')

        # klik button log out
        button_log_out.send_keys(Keys.RETURN)

        # cek redirect ke halaman log in
        self.assertTemplateUsed(response, 'login.html')

    def test_input_status_is_wrong(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story9/')

        # Mencari sebuah object di html berdasarkan id/class/name
        form_username = selenium.find_element_by_id('username')
        form_email = selenium.find_element_by_id('email')
        form_password = selenium.find_element_by_id('password')
        button_log_in = selenium.find_element_by_id('login')

        # isi form dengan data
        form_username.send_keys('test')
        form_email.send_keys('test@mail.com')
        form_password.send_keys('test1234')

        # post data formnya
        button_log_in.send_keys(Keys.RETURN)

        # Check hasil yang dikembalikan
        self.assertIn('Username/email/password salah', selenium.page_source)