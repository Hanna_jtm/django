from django.contrib import admin
from django.urls import re_path
from .views import index, log_in, log_out

#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
	re_path(r'^log_in/$', log_in, name='log_in'),
    re_path(r'^log_out/$', log_out, name='log_out')
]
