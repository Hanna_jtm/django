$(document).ready(function() {
    // %('.submit').click(function(){
    //     var key = $('#search').val();
    //     // alert(key);

    //     $.ajax({
    //         method: 'GET',
    //         url: 'https://www.googleapis.com/books/v1/volumes?q=<keyword>' + key;
    //         success: function(response){
    //             console.log(response);
    //             for(let i=0; i<response.result.length; i++){
    //                 $('#result').append('<p>' + response.result[i].name + '</p>');
    //             }
    //             console.log(response.result);
    //         }
    //     })
    // }
    $('.submit').on("keyup", function(e) {
        q = e.currentTarget.value.toLowerCase()
        console.log(q);

        $.ajax({
            url: "data/?q=" + q,
            datatype: 'json',
            success: function(data){
                $('tbody').html('')
                var result = '<tr>';
                for(var i=0; i<data_search.items.length; i++){
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
					"<td><img class='img-fluid' style='width:22vh' src='" 
					+ data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
					"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
					"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
					"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i 
					+ "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" 
					+ "</td></tr>";
                }
                $('tbody').append(result);
            },
            error: function(error){
                alert("Books not found");
            }
        })
});