"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story4/', include(('story_4.urls', 'story4'), namespace="story_4")),
    path('story6/', include(('story_6.urls', 'story6'), namespace="story_6")),
    path('story7/', include(('story_7.urls', 'story7'), namespace="story_7")),
    path('story8/', include(('story_8.urls', 'story8'), namespace="story_8")),
    path('story9/', include(('story_9.urls', 'story9'), namespace="story_9"))
]
