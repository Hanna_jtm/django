from django.conf.urls import re_path
from .views import views_information, add_information, delete_information

#url for app
urlpatterns = [
	re_path(r'^$', views_information, name='information'),
    re_path(r'^add_information/$', add_information, name='add_information'),
    re_path(r'^(?P<id>[0-9]+)/delete/$', delete_information, name='delete_information')
]