from django.db import models

# Create your models here.
class data_information(models.Model):
    content = models.TextField(blank=True, null=True, default=None)
    dateTime = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.content