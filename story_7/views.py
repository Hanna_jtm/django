from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .forms import forms_information
from .models import data_information
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

response = {}

# Create your views here.
def views_information(request):
    list_status = data_information.objects.all().values()
    response['list_status'] = convert_queryset_into_json(list_status)
    response['form'] = forms_information
    return render(request, 'information.html', response)

def add_information(request):
    form = forms_information(request.POST)
    if request.method == "POST" and form.is_valid():
        print(request.POST)
        status = data_information(content=request.POST['status'])
        status.save()
    return HttpResponseRedirect("/story7/")

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def delete_information(request, id=None):
	todoObject = get_object_or_404(data_information, pk=id)
	todoObject.delete()
	return HttpResponseRedirect('/story7/')