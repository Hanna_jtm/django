from django import forms
from .models import data_information

class forms_information(forms.Form):
    attrs={
        'class' : 'form-control',
        'placeholder' : "write your status here...",
        'rows' : 6,
        'cols' : 25
    }

    status = forms.CharField(label='', required=False, max_length=300, widget=forms.Textarea(attrs=attrs))