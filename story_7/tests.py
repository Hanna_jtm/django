from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import add_information
from .models import data_information
from .forms import forms_information
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Unit Test
class UnitTest(TestCase):
    def test_url_slash_story7_slash_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_url_slash_story7_slash_add_status_is_exist(self):
        status = data_information.objects.create(content="test /story7/add_information is exist")
        response = Client().get('/story7/add_information')
        self.assertEqual(response.status_code, 301)

    def test_forms_information(self):
        information = {'information':'information'}
        form = forms_information(data=information)
        self.assertTrue(form.is_valid())

    def test_button_post_is_exist(self):
        status = data_information.objects.create(content="test button post is exist")
        response = Client().get('/story7/')
        content = response.content.decode('utf8')
        self.assertIn("post", content) #check button berdasarkan id

    def test_button_delete_is_exist(self):
        status = data_information.objects.create(content="test button delete is exist")
        response = Client().get('/story7/')
        content = response.content.decode('utf8')
        self.assertIn("delete", content) #check button berdasarkan id

    def test_models_information(self):
        status = data_information.objects.create(content="test ketepatan pembuatan object")
        count_all_available_status = data_information.objects.all().count()
        self.assertEqual(count_all_available_status, 1)
        self.assertEqual('test ketepatan pembuatan object', str(status))

    def test_views_function_delete_information(self):
        status = data_information.objects.create(content="test function delete information")
        response = Client().get('/story7/' + str(status.pk) + '/delete/')
        self.assertEqual(response.status_code, 302)

    def test_views_function_add_information(self):
        response = Client().post('/story7/add_information/',{'status':'status'})
        self.assertEqual(response.status_code, 302)

# Functional Test
class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story7/')

        # Mencari sebuah object di html berdasarkan id/class/name
        form = selenium.find_element_by_id('id_status')
        button_post = selenium.find_element_by_id('post')

        # isi form dengan data
        form.send_keys('Check Functional Test')

        # post data formnya
        button_post.send_keys(Keys.RETURN)
        
        # Check hasil yang dikembalikan
        self.assertIn('Check Functional Test', selenium.page_source)

        # Check button delete
        button_delete = selenium.find_element_by_id('delete')

    def test_theme_changed(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story7/')

        # Cek implementasi background
        background = selenium.find_element_by_tag_name('body').value_of_css_property('background')
        self.assertIn('rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box', background)

        # Cek implementasi switch/slider
        selenium.find_element_by_class_name("toggle").click()
        background = selenium.find_element_by_tag_name('body').value_of_css_property('background')
        self.assertIn('rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box', background)

    def test_accordion_one(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story7/')

        # Mencari sebuah object di html berdasarkan id/class/name
        form = selenium.find_element_by_id('id_status')
        button_post = selenium.find_element_by_id('post')

        # isi form dengan data
        form.send_keys('Check Functional Test')

        # post data formnya
        button_post.send_keys(Keys.RETURN)

        # Menjalankan js sesuai id
        selenium.find_element_by_id("option1").click()
        
        # Check hasil yang dikembalikan
        self.assertIn('Check Functional Test', selenium.page_source)

    def test_accordion_two(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story7/')

        # Menjalankan js sesuai id
        selenium.find_element_by_id("option2").click()

        # Check hasil yang dikembalikan
        self.assertIn('Staff of Human Resource Development Beruau at BEM Faculty of Computer Science 2019', selenium.page_source)

    def test_accordion_tree(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story7/')

        # Menjalankan js sesuai id
        selenium.find_element_by_id("option3").click()

        # Check hasil yang dikembalikan
        self.assertIn('Staff of Event Division PMB Faculty of Computer Science 2019', selenium.page_source)


