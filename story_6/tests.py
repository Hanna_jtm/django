from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import add_status
from .models import data_status
from .forms import forms_status
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Unit Test
class UnitTest(TestCase):
    def test_url_slash_story6_slash_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_url_slash_story6_slash_add_status_is_exist(self):
        status = data_status.objects.create(content="test /story6/add_status is exist")
        response = Client().get('/story6/add_status')
        self.assertEqual(response.status_code, 301)

    def test_forms_status(self):
        status = {'status':'status'}
        form = forms_status(data=status)
        self.assertTrue(form.is_valid())

    def test_button_post_is_exist(self):
        status = data_status.objects.create(content="test button post is exist")
        response = Client().get('/story6/')
        content = response.content.decode('utf8')
        self.assertIn("post", content) #check button berdasarkan id

    def test_button_delete_is_exist(self):
        status = data_status.objects.create(content="test button delete is exist")
        response = Client().get('/story6/')
        content = response.content.decode('utf8')
        self.assertIn("delete", content) #check button berdasarkan id

    def test_models_status(self):
        status = data_status.objects.create(content="test ketepatan pembuatan object")
        count_all_available_status = data_status.objects.all().count()
        self.assertEqual(count_all_available_status, 1)
        self.assertEqual('test ketepatan pembuatan object', str(status))

    def test_views_function_delete_status(self):
        status = data_status.objects.create(content="test function delete status")
        response = Client().get('/story6/' + str(status.pk) + '/delete/')
        self.assertEqual(response.status_code, 302)

    def test_views_function_add_status(self):
        response = Client().post('/story6/add_status/',{'status':'status'})
        self.assertEqual(response.status_code, 302)

# Functional Test
class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story6/')

        # Mencari sebuah object di html berdasarkan id/class/name
        form = selenium.find_element_by_id('id_status')
        button_post = selenium.find_element_by_id('post')

        # isi form dengan data
        form.send_keys('Check Functional Test')

        # post data formnya
        button_post.send_keys(Keys.RETURN)
        
        # Check hasil yang dikembalikan
        self.assertIn('Check Functional Test', selenium.page_source)

        # Check button delete
        button_delete = selenium.find_element_by_id('delete')