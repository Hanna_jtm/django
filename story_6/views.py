from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .forms import forms_status
from .models import data_status
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

response = {}

# Create your views here.
def views_status(request):
    list_status = data_status.objects.all().values()
    response['list_status'] = convert_queryset_into_json(list_status)
    response['form'] = forms_status
    return render(request, 'status.html', response)

def add_status(request):
    form = forms_status(request.POST)
    if request.method == "POST" and form.is_valid():
        print(request.POST)
        status = data_status(content=request.POST['status'])
        status.save()
    return HttpResponseRedirect("/story6/")

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def delete_status(request, id=None):
	todoObject = get_object_or_404(data_status, pk=id)
	todoObject.delete()
	return HttpResponseRedirect('/story6/')