from django.conf.urls import re_path
from .views import views_status, add_status, delete_status

#url for app
urlpatterns = [
	re_path(r'^$', views_status, name='status'),
    re_path(r'^add_status/$', add_status, name='add_status'),
    re_path(r'^(?P<id>[0-9]+)/delete/$', delete_status, name='delete_status')
]