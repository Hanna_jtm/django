# Generated by Django 2.2.5 on 2019-10-06 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='time',
            field=models.TimeField(),
        ),
    ]
