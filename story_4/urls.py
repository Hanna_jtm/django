from django.conf.urls import re_path
from .views import index, activities, add_message, delete_todo

#url for app
urlpatterns = [
	re_path(r'^$', index, name='index'),
	re_path(r'^activities/$',  activities, name='activities'),
	re_path(r'^add_message/$', add_message, name='add_message'),
	re_path(r'^(?P<id>[0-9]+)/delete/$', delete_todo, name='delete_todo')
]