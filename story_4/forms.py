from django import forms

class Add_ToDo(forms.Form):

	attrs = {
        'class': 'form-control',
    }

	date = forms.DateField(label='date', required=False, widget=forms.DateInput(attrs=attrs))
	time = forms.TimeField(label = 'time', required=False, widget=forms.DateTimeInput(attrs=attrs))
	categories = forms.CharField(label ='categories', required=False, widget=forms.TextInput(attrs=attrs))
	activities = forms.CharField(label = 'activities', required=False, widget=forms.TextInput(attrs=attrs))
	place = forms.CharField(label = 'place', required=False, widget=forms.TextInput(attrs=attrs))
