from django.conf.urls import re_path
from .views import index, data

#url for app
urlpatterns = [
	re_path(r'^$', index, name='index'),
	re_path(r'^data/$', data, name='data')
]