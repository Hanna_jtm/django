from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_url_slash_story8_slash_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_slash_story8_slash_data_slash_is_exist(self):
        response = Client().get('/story8/data/')
        self.assertEqual(response.status_code, 200)

    def test_views_function_index_use_correct_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed("search.html")

    def test_seacrh_bar_is_exist(self):
        response = Client().get('/story8/').content.decode('utf-8')
        self.assertIn("input", response)
    
    def test_table_is_exist(self):
        response = Client().get('/story8/').content.decode('utf-8')
        self.assertIn('table', response)

# Functional Test
class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_list_book_is_exist_when_open_the_website(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story8/')
        time.sleep(10)

        # Mencari sebuah object di html berdasarkan id/class/name
        selenium.find_element_by_id("textField").send_keys(Keys.RETURN)
        # time.sleep(10)
        # selenium.refresh()
        # Check hasil yang dikembalikan
        self.assertIn('Computer', selenium.page_source)

    def test_input_search(self):
        selenium = self.selenium
        # Membuka link yang akan di test
        selenium.get('http://127.0.0.1:8000/story8/')
        time.sleep(15)

        # Mencari sebuah object di html berdasarkan id/class/name
        search = selenium.find_element_by_id("textField")

        # isi form dengan dataeys
        search.send_keys('science')

        # post data formnya
        search.send_keys(Keys.RETURN)
        # time.sleep(10)
        # selenium.refresh()        
        # Check hasil yang dikembalikan
        self.assertIn('Steal this Computer Book 3', selenium.page_source)

